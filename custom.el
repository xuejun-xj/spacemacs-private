(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-idle-delay 0.08)
 '(company-minimum-prefix-length 1)
 '(custom-safe-themes
   '("78e6be576f4a526d212d5f9a8798e5706990216e9be10174e3f3b015b8662e27" default))
 '(global-hungry-delete-mode t)
 '(helm-dictionary-browser-function 'browse-url-default-windows-browser)
 '(helm-dictionary-database "/usr/share/trans/de-en")
 '(package-selected-packages
   '(find-file-in-repository popwin smartparens counsel swiper smex hungry-delete company))
 '(popwin:popup-window-position 'right)
 '(warning-suppress-log-types '((use-package) (use-package) ((package reinitialization))))
 '(warning-suppress-types '((use-package) ((package reinitialization)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
